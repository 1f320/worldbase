package db

import (
	"embed"

	"log"

	"emperror.dev/errors"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	migrate "github.com/rubenv/sql-migrate"

	// SQLite driver
	_ "modernc.org/sqlite"
)

// SupportedVersion is the supported version.
// As in, *major* version, one that involves a complete reset of the database schema (or switching storage entirely). Migrations are applied automatically as long as the supported version is correct.
const SupportedVersion = 1

//go:embed migrations
var fs embed.FS

// Errors
const (
	ErrNoRowsAffected      = errors.Sentinel("no rows affected")
	ErrVersionNotSupported = errors.Sentinel("database version not supported")
)

// DB is a database file
type DB struct {
	*sqlx.DB
}

// New opens a database and returns the object.
func New(dsn string) (*DB, error) {
	conn, err := sqlx.Open("sqlite", dsn)
	if err != nil {
		return nil, errors.Wrap(err, "opening database")
	}

	db := &DB{
		DB: conn,
	}

	err = db.Ping()
	if err != nil {
		return nil, errors.Wrap(err, "pinging database")
	}

	// enable foreign keys
	_, err = db.Exec("PRAGMA foreign_keys = true")
	if err != nil {
		return nil, errors.Wrap(err, "enabling foreign keys")
	}

	migrations := &migrate.EmbedFileSystemMigrationSource{
		FileSystem: fs,
		Root:       "migrations",
	}

	n, err := migrate.Exec(conn.DB, "sqlite3", migrations, migrate.Up)
	if err != nil {
		return nil, errors.Wrap(err, "running migrations")
	}

	if n != 0 {
		log.Printf("Performed %v migrations on %v!", n, dsn)
	}

	err = db.createSystemUser()
	if err != nil {
		return nil, errors.Wrap(err, "creating system user")
	}

	err = db.deleteExpiredSessions()
	if err != nil {
		return nil, errors.Wrap(err, "deleting expired sessions")
	}

	return db, nil
}

func (db *DB) createSystemUser() error {
	sql, args, err := sq.Insert("users").Columns("id", "name", "password", "salt", "flags").Values(uuid.Nil, "system", []byte{0}, []byte{0}, UserFlagAdmin).Suffix("ON CONFLICT (id) DO NOTHING").ToSql()
	if err != nil {
		return err
	}

	_, err = db.Exec(sql, args...)
	return err
}
