package db

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"log"
	"regexp"
	"time"

	"emperror.dev/errors"
	"gitlab.com/1f320/worldbase/common"
	"golang.org/x/crypto/bcrypt"

	sq "github.com/Masterminds/squirrel"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/google/uuid"
)

// User is a user.
type User struct {
	ID   uuid.UUID `db:"id"`
	Name string    `db:"name"`

	// Hashed password, not plain text!
	Password []byte `db:"password"`
	Salt     string `db:"salt"`

	Flags UserFlags `db:"flags"`
}

func (u User) IsEditor() bool {
	return u.Flags&UserFlagEditor == UserFlagEditor
}

func (u User) IsAdmin() bool {
	return u.Flags&UserFlagAdmin == UserFlagAdmin
}

func (u User) Committer() *object.Signature {
	return &object.Signature{
		Name:  u.Name,
		Email: u.ID.String() + "@noreply.wiki.local",
		When:  time.Now().UTC(),
	}
}

// CheckPassword checks the user's hashed password against its possible plaintext equivalent.
func (u User) CheckPassword(password string) error {
	if u.ID == uuid.Nil {
		return ErrSystemUser
	}

	return bcrypt.CompareHashAndPassword(u.Password, []byte(password+u.Salt))
}

// UserFlags are flags applied to the user.
type UserFlags uint

const (
	// FlagAdmin overrides all permissions.
	UserFlagAdmin UserFlags = 1 << 0
	// FlagEditor indicates the user can edit all pages.
	UserFlagEditor UserFlags = 1 << 1

	UserFlagAll UserFlags = UserFlagAdmin | UserFlagEditor
)

var usernameRe = regexp.MustCompile(`^[\w-]{1,32}$`)

const ErrInvalidUsername = errors.Sentinel("invalid username")
const ErrSystemUser = errors.Sentinel("user is system user, cannot log in")

// CreateUser creates a new user.
func (db *DB) CreateUser(ctx context.Context, name string, password []byte, isAdmin, isEditor bool) (*User, error) {
	if !usernameRe.MatchString(name) {
		return nil, ErrInvalidUsername
	}

	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		// if we can't generate random bytes, panic
		log.Panicf("Error reading random bytes: %v", err)
	}

	salt := base64.URLEncoding.EncodeToString(b)

	hash, err := bcrypt.GenerateFromPassword(append(password, salt...), bcrypt.DefaultCost)
	if err != nil {
		return nil, errors.Wrap(err, "db.CreateUser: hash password")
	}

	var flags UserFlags
	if isAdmin {
		flags |= UserFlagAdmin
	}
	if isEditor {
		flags |= UserFlagEditor
	}

	sql, args, err := sq.Insert("users").Columns("id", "name", "password", "flags", "salt").Values(uuid.New(), name, hash, flags, salt).Suffix("RETURNING *").ToSql()
	if err != nil {
		return nil, err
	}

	var u User
	err = db.GetContext(ctx, &u, sql, args...)
	if err != nil {
		return nil, errors.Wrap(err, "db.CreateUser: insert")
	}
	return &u, err
}

// ErrUserNotFound ...
const ErrUserNotFound = errors.Sentinel("no user with that name found")

// User returns a user by username.
func (db *DB) User(ctx context.Context, name string) (*User, error) {
	q, args, err := sq.Select("*").From("users").Where(sq.Eq{"name": name}).ToSql()
	if err != nil {
		return nil, err
	}

	var u User
	err = db.GetContext(ctx, &u, q, args...)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, ErrUserNotFound
		}
		return nil, errors.Wrap(err, "db.User: select")
	}
	return &u, err
}

// Users returns all users in the database.
func (db *DB) Users(ctx context.Context) ([]User, error) {
	sql, args, err := sq.Select("id", "name", "flags").From("users").OrderBy("id").ToSql()
	if err != nil {
		return nil, err
	}

	var u []User
	err = db.SelectContext(ctx, &u, sql, args...)
	if err != nil {
		return nil, errors.Wrap(err, "db.Users: select")
	}
	return u, err
}

func (db *DB) UserFromCookie(ctx context.Context, cookie string) (u User, err error) {
	err = db.GetContext(ctx, &u, "SELECT * FROM users WHERE id = (SELECT user_id FROM sessions WHERE cookie = $1 AND expires > $2)", cookie, time.Now().Unix())
	if errors.Cause(err) == sql.ErrNoRows {
		return u, ErrUserNotFound
	}
	return u, err
}

const CookieExpiry = 30 * 24 * time.Hour

func (db *DB) CreateUserCookie(ctx context.Context, u User) (cookie string, err error) {
	cookie = common.RandBase64(32)

	_, err = db.ExecContext(ctx, "INSERT INTO sessions (user_id, cookie, expires) VALUES ($1, $2, $3)", u.ID, cookie, time.Now().Add(CookieExpiry))
	if err != nil {
		return "", err
	}
	return cookie, nil
}

func (db *DB) DeleteSession(ctx context.Context, id uuid.UUID, value string) error {
	_, err := db.ExecContext(ctx, "DELETE FROM sessions WHERE user_id = $1 AND cookie = $2", id, value)
	return err
}

func (db *DB) deleteExpiredSessions() error {
	_, err := db.Exec("DELETE FROM sessions WHERE expires < $1", time.Now().Unix())
	return err
}
