package auth

import (
	"net/http"

	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/db"
	"gitlab.com/1f320/worldbase/server"
)

func (r *Router) getPreferences(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	u, ok := server.UserFromContext(ctx)
	if !ok {
		http.Redirect(w, req,
			r.Redirect("/auth/login?error=not-logged-in"), http.StatusTemporaryRedirect,
		)
	}

	tctx, err := r.File.TemplateContext("preferences")
	if err != nil {
		common.Log.Errorf("Error creating template context: %v", err)
	}

	tctx.User = &u
	tctx.Title = "Preferences"
	tctx.Active = "preferences"

	pctx := prefsContext{
		User: u,
	}

	err = tctx.ExecTemplate(w, pctx)
	if err != nil {
		common.Log.Errorf("Error executing preferences template: %v", err)
	}
}

type prefsContext struct {
	User db.User
}
