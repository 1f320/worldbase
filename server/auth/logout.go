package auth

import (
	"net/http"
	"time"

	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/server"
)

func (r *Router) getLogout(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	u, ok := server.UserFromContext(ctx)
	if !ok {
		// can't log out if you're already logged out
		http.Redirect(w, req, r.Redirect("/index?error=already-logged-out"), http.StatusTemporaryRedirect)
		return
	}

	cookie, err := req.Cookie(common.SessionCookieName)
	if err == nil {
		err = r.File.DB.DeleteSession(ctx, u.ID, cookie.Value)
		if err != nil {
			// this isn't *that* big a problem because expired sessions are deleted on startup
			common.Log.Errorf("Error deleting session for %v: %v", u.ID, err)
		}
	}

	http.SetCookie(w, &http.Cookie{
		Name:    common.SessionCookieName,
		Value:   "",
		Expires: time.Now(),
		Path:    "/",
	})

	http.Redirect(w, req, r.Redirect("/index?error=successfully-logged-out"), http.StatusTemporaryRedirect)
}
