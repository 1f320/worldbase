package auth

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/1f320/worldbase/server"
)

// Router is the page router.
type Router struct {
	*server.Router
}

// Mount mounts this router.
func Mount(s *server.Router) {
	r := &Router{s}

	r.Route("/auth", func(sub chi.Router) {
		sub.HandleFunc("/login", r.getLogin)
		sub.Post("/login/callback", r.postLogin)
		sub.Get("/logout", r.getLogout)

		sub.HandleFunc("/preferences", r.getPreferences)
		sub.HandleFunc("/preferences/authentication", nil)
		sub.HandleFunc("/preferences/invites", nil)
		sub.HandleFunc("/preferences/sessions", nil)
	})
}
