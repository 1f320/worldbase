package server

import (
	"context"

	"gitlab.com/1f320/worldbase/db"
)

type ctxKey int

var (
	ctxKeyUser ctxKey = 1
)

func UserFromContext(ctx context.Context) (u db.User, ok bool) {
	iface := ctx.Value(ctxKeyUser)
	if iface == nil {
		return u, false
	}

	v, ok := iface.(db.User)
	if ok {
		return v, true
	}
	return u, false
}

func ContextWithUser(ctx context.Context, u db.User) context.Context {
	return context.WithValue(ctx, ctxKeyUser, u)
}
