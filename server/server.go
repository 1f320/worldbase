package server

import (
	"net/http"
	"regexp"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/file"
)

// Router is the root router.
type Router struct {
	chi.Router

	File *file.File
}

// New creates a new Router.
func New(f *file.File) *Router {
	r := &Router{
		Router: chi.NewMux(),
		File:   f,
	}

	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Use(r.MaybeAuth)

	r.Get("/about.json", func(w http.ResponseWriter, r *http.Request) {
		render.JSON(w, r, aboutData{
			Using:   "worldbase",
			Version: common.Version,
			Name:    f.Config.Name,
		})
	})

	r.Mount("/static", common.StaticServer)

	r.NotFound(func(w http.ResponseWriter, req *http.Request) {
		http.Redirect(w, req, r.Redirect("/index?error=404"), http.StatusTemporaryRedirect)
	})

	return r
}

var slashRegexp = regexp.MustCompile(`\/+`)

// Redirect returns a redirect URL for the given path.
// This path should start with a /.
func (r *Router) Redirect(path string) string {
	path = r.File.Config.Data.RoutePrefix + path

	return slashRegexp.ReplaceAllLiteralString(path, "/")
}

type aboutData struct {
	Using   string `json:"using"`
	Version string `json:"version"`
	Name    string `json:"name"`
}
