package file

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"emperror.dev/errors"
	"github.com/go-git/go-git/v5"
	"gitlab.com/1f320/worldbase/db"
)

type InvalidPageNameError struct {
	Prefix string
	Suffix string
}

func (e *InvalidPageNameError) Error() string {
	if e.Prefix != "" && e.Suffix != "" {
		return fmt.Sprintf("page name cannot start with %q and cannot end with %q", e.Prefix, e.Suffix)
	}

	if e.Prefix != "" {
		return fmt.Sprintf("page name cannot start with %q", e.Prefix)
	}

	if e.Suffix != "" {
		return fmt.Sprintf("page name cannot end with %q", e.Suffix)
	}

	return "invalid page name"
}

func (f *File) SavePage(name, content string, u db.User, reason string) error {
	if strings.HasPrefix(strings.ToLower(name), "auth") {
		return &InvalidPageNameError{Prefix: "auth"}
	} else if strings.HasPrefix(strings.ToLower(name), "static") {
		return &InvalidPageNameError{Prefix: "static"}
	} else if strings.HasPrefix(strings.ToLower(name), "list") {
		return &InvalidPageNameError{Prefix: "list"}
	} else if strings.HasSuffix(strings.ToLower(name), "/edit") {
		return &InvalidPageNameError{Suffix: "/edit"}
	}

	f.mu.Lock()
	defer f.mu.Unlock()

	path := filepath.Join(f.RepoRoot, name)

	err := verifyDirectoryExists(filepath.Dir(path), false)
	if err != nil {
		return errors.Wrap(err, "f.SavePage: directory")
	}

	err = os.WriteFile(path, []byte(content), 0666)
	if err != nil {
		return errors.Wrap(err, "f.SavePage: writing file")
	}

	_, err = f.Worktree.Add(path)
	if err != nil {
		return errors.Wrap(err, "f.SavePage: staging file")
	}

	if reason == "" {
		reason = "[no reason given]"
	}

	_, err = f.Worktree.Commit(reason, &git.CommitOptions{
		Author: u.Committer(),
	})
	if err != nil {
		return errors.Wrap(err, "f.SavePage: creating commit")
	}

	err = f.reindex()
	if err != nil {
		return errors.Wrap(err, "f.SavePage: reindexing repository")
	}

	return nil
}
