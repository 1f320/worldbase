package file

import (
	"html/template"
	"io"
	"regexp"
	"strings"

	"github.com/russross/blackfriday/v2"
	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/db"
)

type TemplateContext struct {
	f    *File
	tmpl *template.Template

	Site TemplateContextConfig

	Alert, AlertType string

	Title  string
	Active string
	User   *db.User

	InnerTemplate string
	InnerData     interface{}
}

type TemplateContextConfig struct {
	Title string
}

func (f *File) TemplateContext(inner string) (*TemplateContext, error) {
	tctx := &TemplateContext{
		f: f,
		Site: TemplateContextConfig{
			Title: f.Config.Name,
		},
		InnerTemplate: inner,
	}

	_, err := f.Templates(tctx)
	if err != nil {
		return nil, err
	}
	return tctx, nil
}

const mdExtensions = blackfriday.Autolink | blackfriday.Tables | blackfriday.FencedCode | blackfriday.Strikethrough | blackfriday.BackslashLineBreak

func (tctx *TemplateContext) RenderPageMarkdown(content string) template.HTML {
	content = tctx.f.LinkPages(content)

	html := blackfriday.Run([]byte(content), blackfriday.WithExtensions(mdExtensions))

	return template.HTML(html)
}

func (tctx *TemplateContext) ExecTemplate(w io.Writer, data interface{}) error {
	tctx.InnerData = data

	return tctx.tmpl.ExecuteTemplate(w, "index.html", tctx)
}

func (tctx *TemplateContext) CallTemplate() template.HTML {
	var b strings.Builder

	name := tctx.InnerTemplate

	if !strings.HasSuffix(name, ".html") {
		name += ".html"
	}

	err := tctx.tmpl.ExecuteTemplate(&b, name, tctx.InnerData)
	if err != nil {
		common.Log.Errorf("Error executing template %q: %v", name, err)
		return ""
	}
	return template.HTML(b.String())
}

var slashRegexp = regexp.MustCompile(`\/+`)

func (tctx *TemplateContext) FullPath(path string) string {
	path = tctx.f.Config.Data.RoutePrefix + path

	return slashRegexp.ReplaceAllLiteralString(path, "/")
}
