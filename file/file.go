package file

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"sync"

	"emperror.dev/errors"
	"github.com/BurntSushi/toml"
	"github.com/go-git/go-git/v5"
	"gitlab.com/1f320/worldbase/db"
	"go.uber.org/multierr"
)

// File is a worldbase directory containing data and Other Stuff
type File struct {
	mu    sync.RWMutex
	index map[string]string

	Abs           string
	AttachmentDir string
	RepoRoot      string

	Config Config

	DB       *db.DB
	Repo     *git.Repository
	Worktree *git.Worktree
}

// ConfigName is the config file name.
const ConfigName = "world.wbx"

// ErrNotDirectory is returned when the path passed to Open is a file and not a directory
const ErrNotDirectory = errors.Sentinel("path is not a directory")

// Open opens a File, creating it if it doesn't exist.
func Open(dir string) (*File, error) {
	fi, err := os.Stat(dir)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return nil, errors.Wrap(err, "file.Open: stat dir")
		}

		err = os.Mkdir(dir, 0700)
		if err != nil {
			return nil, errors.Wrap(err, "file.Open: mkdir")
		}

		fi, err = os.Stat(dir)
		if err != nil {
			return nil, errors.Wrap(err, "file.Open: stat newly created dir")
		}
	}

	if !fi.IsDir() {
		dir = filepath.Dir(dir)
	}

	abs, err := filepath.Abs(dir)
	if err != nil {
		return nil, errors.Wrap(err, "file.Open: absolute path")
	}

	cpath := path.Join(abs, ConfigName)

	_, err = os.Stat(cpath)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return nil, errors.Wrap(err, "file.Open: stat config file")
		}

		f, err := os.Create(cpath)
		if err != nil {
			return nil, errors.Wrap(err, "file.Open: create config file")
		}
		defer func() {
			err2 := f.Close()
			err = multierr.Combine(err, err2)
		}()

		enc := toml.NewEncoder(f)
		enc.Indent = ""
		err = enc.Encode(defConfig)
		if err != nil {
			return nil, errors.Wrap(err, "file.Open: encode default config")
		}
	}

	b, err := os.ReadFile(cpath)
	if err != nil {
		return nil, errors.Wrap(err, "file.Open: read config file")
	}

	var conf Config
	err = toml.Unmarshal(b, &conf)
	if err != nil {
		return nil, errors.Wrap(err, "file.Open: unmarshal config file")
	}

	if conf.Data.Version != db.SupportedVersion {
		return nil, errors.Sentinel(fmt.Sprintf("Invalid data version: expected version %v, got version %v.", db.SupportedVersion, conf.Data.Version))
	}

	fd := &File{
		index: map[string]string{},

		Abs:           abs,
		AttachmentDir: path.Join(abs, "attachments"),
		RepoRoot:      path.Join(abs, "pages"),
		Config:        conf,
	}

	err = fd.createAttachmentsDir()
	if err != nil {
		return nil, err
	}

	err = fd.setupGit()
	if err != nil {
		return nil, err
	}

	fd.DB, err = db.New(path.Join(abs, conf.Data.Database))
	if err != nil {
		return nil, errors.Wrap(err, "file.Open: creating database")
	}

	err = fd.reindex()
	if err != nil {
		return nil, errors.Wrap(err, "file.Open: indexing database")
	}

	return fd, nil
}

// createAttachmentsDir creates the f.Abs/attachments directory if it doesn't exist.
func (f *File) createAttachmentsDir() error {
	p := path.Join(f.Abs, "attachments")

	err := verifyDirectoryExists(p, true)
	if err != nil {
		return errors.Wrap(err, "f.createAttachmentsDir: verifying attachments directory exists")
	}

	if _, err = os.Stat(path.Join(p, ".empty")); errors.Is(err, os.ErrNotExist) {
		fi, err := os.Create(path.Join(p, ".empty"))
		if err != nil {
			return errors.Wrap(err, "f.createAttachmentsDir: create .empty file")
		}
		err = fi.Close()
		if err != nil {
			return errors.Wrap(err, "f.createAttachmentsDir: fi.Close")
		}
	}
	return nil
}
