package file

import (
	"embed"
	"path/filepath"
	"strings"
)

//go:embed default_pages/*.md
var defaultPagesFS embed.FS

type initialPageData struct {
	Name    string
	Content []byte
}

var defaultPages []initialPageData

func init() {
	dir, err := defaultPagesFS.ReadDir("default_pages")
	if err != nil {
		panic(err)
	}

	for _, e := range dir {
		if e.IsDir() {
			continue
		}

		if !strings.HasSuffix(e.Name(), ".md") {
			continue
		}

		b, err := defaultPagesFS.ReadFile(filepath.Join("default_pages", e.Name()))
		if err != nil {
			panic(err)
		}

		defaultPages = append(defaultPages, initialPageData{
			// we don't need a super complicated directory structure in the default_pages directory, so we do this instead
			Name: strings.ReplaceAll(
				e.Name(),
				"__", "/",
			),
			Content: b,
		})
	}
}
