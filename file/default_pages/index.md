# Index

Welcome to your new Worldbase wiki!

If you're new to using Markdown, check out the [[syntax guide|worldbase/syntax]].

## Useful links

- [Source code](https://gitlab.com/1f320/worldbase)
