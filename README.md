# worldbase

Mainly a worldbuilding wiki thing, with optional authentication.

Intended to be somewhat similar to [Zim](https://zim-wiki.org/) or [Cherrytree](https://www.giuspen.com/cherrytree/) (but worse!)

## Running

\[currently not actually runnable]

## Format

worldbase stores data in a single directory.

```
<root directory>
├── attachments/
│   ├── <hash>.<extension>  (single attachments)
│   └── ...
├── pages/                  (git repository with pages)
│   ├── .git/
│   ├── index.md
│   └── ...
├── data.db                 (database file)
└── world.toml              (configuration file)
```
