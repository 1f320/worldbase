CGO_ENABLED=0 go build -v -o worldbase.out -ldflags="-buildid= -X gitlab.com/1f320/worldbase/common.Version=`git rev-parse --short HEAD`" ./cmd/
strip worldbase.out
