package web

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/urfave/cli/v2"
	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/file"
	"gitlab.com/1f320/worldbase/server"
	"gitlab.com/1f320/worldbase/server/auth"
	"gitlab.com/1f320/worldbase/server/page"
)

// Command is the serve command.
var Command = &cli.Command{
	Name:    "web",
	Aliases: []string{"serve"},
	Usage:   "Run the worldbase server",
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:    "debug",
			Aliases: []string{"D"},
			Usage:   "Whether to enable debug logging",
			Value:   true,
			EnvVars: []string{"WORLDBASE_DEBUG"},
		},
		&cli.BoolFlag{
			Name:    "log-json",
			Aliases: []string{"J"},
			Usage:   "Whether to log as JSON",
			Value:   false,
			EnvVars: []string{"WORLDBASE_LOG_JSON"},
		},
		&cli.IntFlag{
			Name:    "port",
			Aliases: []string{"p"},
			Usage:   "The port to listen on",
			Value:   8900,
			EnvVars: []string{"WORLDBASE_PORT"},
		},
	},
	Action: run,
}

func run(c *cli.Context) (err error) {
	common.SetupLog(c.Bool("log-json"), c.Bool("debug"))

	f, err := file.Open(c.String("database"))
	if err != nil {
		return cli.Exit(fmt.Sprintf("Error opening database: %v", err), 1)
	}

	r := server.New(f)
	page.Mount(r)
	auth.Mount(r)

	port := ":" + strconv.Itoa(c.Int("port"))

	e := make(chan error)

	go func() {
		e <- http.ListenAndServe(port, r)
	}()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)

	common.Log.Infof("Listening on %v!", port)
	common.Log.Infof("Press Ctrl-C or send an interrupt signal to stop.")

	select {
	case <-sc:
		common.Log.Infof("Interrupt signal received. Shutting down...")
		if err = f.DB.Close(); err != nil {
			common.Log.Errorf("Error closing database: %v", err)
		} else {
			common.Log.Infof("Closed database")
		}
	case err := <-e:
		return cli.Exit(err, 1)
	}

	return nil
}
