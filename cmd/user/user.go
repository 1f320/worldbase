package user

import (
	"fmt"
	"io"
	"log"

	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/1f320/worldbase/file"
)

// Command is the users command.
var Command = &cli.Command{
	Name:    "user",
	Aliases: []string{"users"},
	Usage:   "List and manage user accounts",
	Action:  runList,

	Subcommands: []*cli.Command{
		createUserCommand,
	},
}

func runList(c *cli.Context) (err error) {
	// silence log output
	log.SetOutput(io.Discard)

	dbName := c.String("database")

	f, err := file.Open(dbName)
	if err != nil {
		return cli.Exit(fmt.Sprintf("Error opening database: %v", err), 1)
	}

	users, err := f.DB.Users(c.Context)
	if err != nil {
		return cli.Exit(fmt.Sprintf("Error fetching users: %v", err), 1)
	}

	if len(users) == 0 {
		return cli.Exit("There are no registered users.", 0)
	}

	table := tablewriter.NewWriter(c.App.Writer)
	table.SetHeader([]string{"ID", "Name", "Is editor?", "Is admin?"})
	table.SetBorder(false)

	for _, u := range users {
		table.Append([]string{
			fmt.Sprint(u.ID),
			u.Name,
			yesNo(u.IsEditor()),
			yesNo(u.IsAdmin()),
		})
	}

	table.Render()

	return
}

func yesNo(b bool) string {
	if b {
		return "yes"
	}
	return "no"
}
