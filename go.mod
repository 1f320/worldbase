module gitlab.com/1f320/worldbase

go 1.16

require (
	emperror.dev/errors v0.8.0
	github.com/BurntSushi/toml v0.3.1
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/Masterminds/squirrel v1.5.0
	github.com/go-chi/chi/v5 v5.0.4
	github.com/go-chi/render v1.0.1
	github.com/go-git/go-git/v5 v5.4.2
	github.com/google/uuid v1.1.2
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.4.0
	github.com/logrusorgru/aurora/v3 v3.0.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/rubenv/sql-migrate v0.0.0-20210614095031-55d5740dbbcc
	github.com/russross/blackfriday/v2 v2.0.1
	github.com/urfave/cli/v2 v2.3.0
	github.com/ziutek/mymysql v1.5.4 // indirect
	go.uber.org/multierr v1.6.0
	go.uber.org/zap v1.10.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	golang.org/x/tools v0.1.0 // indirect
	modernc.org/sqlite v1.10.8
)
